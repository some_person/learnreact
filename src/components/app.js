import React, {Fragment} from 'react';
import Header from "./includes/header";
import Footer from "./includes/footer";
import Home from "./pages/home";

export default function App() {
    return <Fragment>
        <Header myKey="myKey"/>
        <Home />
        <Footer />
    </Fragment>
}