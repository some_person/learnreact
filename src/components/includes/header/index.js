import "./scss/header.scss";

import React, {Fragment, Component} from "react";

export default class Header extends React.Component {

    constructor() {
        super();
        this.state = {
          isActive: false
        };
    }

    render() {
        return (
            <Fragment>
                <header className="header">
                    <div className="container">
                        <div className="header__body">
                            <div className={`header__burger ${this.state.isActive ? 'active' : ''}`} onClick={this.handleChangeState.bind(this)}>
                                <span></span>
                            </div>
                            <ul className={`header__list ${this.state.isActive ? 'active' : ''}`}>
                                <li>
                                    <a className="header__link" href="#!">Home</a>
                                </li>
                                <li>
                                    <a className="header__link" href="#!">Новости</a>
                                </li>
                                <li>
                                    <a className="header__link" href="#!">Каталог</a>
                                </li>
                                <li>
                                    <a className="header__link" href="#!">Контакты</a>
                                </li>
                                <li>
                                    <a className="header__link" href="#!">О нас</a>
                                </li>
                            </ul>
                            <div className="header__subscribe subscribe">
                                <a href="#!" className="subscribe__link">subscribe</a>
                            </div>
                        </div>

                    </div>

                </header>
            </Fragment>
        )
    }

    handleChangeState() {
        this.setState({isActive: !this.state.isActive});
        console.log(this.state.isActive)
    }
}