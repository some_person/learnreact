import "./scss/footer.scss";

import React, {Fragment} from "react";

export default function Footer() {
    return <Fragment>
        <footer className="footer">
            <div className="container">
                <p className="footer__text">
                    @Powered by Some Person
                </p>
            </div>
        </footer>
    </Fragment>
}