import React, {Fragment} from 'react';
import Item from './item';

export default function Home() {
    return <Fragment>
        <section className="main">
            <div className="container">
                <div className={"main__items items"}>
                    <Item imgHref={"https://tut.by"} imgSrc={"./images/01.png"} />
                    <Item imgHref={"https://tut.by"} imgSrc={"./images/02.jpg"} />
                    <Item />
                    <Item imgHref={"https://tut.by"} imgSrc={"./images/03.jpg"} />
                    <Item />
                    <Item />
                    <Item imgHref={"https://tut.by"} imgSrc={"./images/04.jpg"} />
                    <Item />
                    <Item imgHref={"https://tut.by"} imgSrc={"./images/02.jpg"} />
                    <Item imgHref={"https://tut.by"} imgSrc={"./images/03.jpg"} />
                    <Item />
                </div>
            </div>
        </section>
    </Fragment>

}
