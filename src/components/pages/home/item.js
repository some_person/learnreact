import './scss/item.scss';


import React, {Fragment} from 'react';

export default function Item(props) {
    let {imgHref, imgSrc} = props;
    let defaultSrc = "./images/default.png";
    return <Fragment>
        {
            <div className={"items__item item"}>
                <a href={imgHref !== undefined ? imgHref : "https://google.by"}>
                    <img src={imgSrc !== undefined ? imgSrc : defaultSrc } alt="my item Image"/>
                </a>
            </div>
        }
    </Fragment>
}